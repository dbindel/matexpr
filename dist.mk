all: dist

# === Version information ===

VERS=0.18
MATEXPR=matexpr-$(VERS)

include make.inc
include Makefile

# === Tag targets ===

tag:
	svn copy -m "Tag $(VERS)." \
		file:///scratch/svn/matexpr/trunk \
		file:///scratch/svn/matexpr/tags/$(VERS)

# === Tar / distribution targets ===

SRC=\
	README Makefile make.inc matexpr.pdf doc/matexpr.tex \
	src/Makefile src/matexpr.y src/matexpr.l \
	src/*.hh src/*.h src/*.c src/*.cc \
	example/Makefile example/*.cc \
	testing/testbad.in testing/test-in.cc testing/Makefile

README: README.in dist.mk
	echo $(MATEXPR) > tmp.txt
	cat tmp.txt README.in > README
	rm tmp.txt

doc/index.html: doc/index0.html dist.mk
	cat doc/index0.html | \
	sed s:matexpr\\.tar\\.gz:$(MATEXPR).tar.gz: >doc/index.html

matexpr.pdf: doc/matexpr.tex
	(cd doc; make matexpr.pdf; cp matexpr.pdf ..)

dist: tgz
	tar -xzf ../$(MATEXPR).tar.gz
	(cd $(MATEXPR); make bin test demo)
	rm -rf $(MATEXPR)

tgz: $(MATEXPR).tar.gz

$(MATEXPR).tar.gz: clean src/matexpr.cc src/lex.yy.c matexpr.pdf README
	ls $(SRC) | sed s:^:$(MATEXPR)/: >MANIFEST
	(ln -s `pwd` ../$(MATEXPR))
	(cd ..; tar -czvf $(MATEXPR).tar.gz `cat $(MATEXPR)/MANIFEST`)
	(cd ..; rm $(MATEXPR))

upload: dist doc/index.html matexpr.pdf
	scp ../$(MATEXPR).tar.gz doc/index.html matexpr.pdf \
		access:public_html/matexpr
	ssh access chmod a+r \
		public_html/matexpr/$(MATEXPR).tar.gz \
		public_html/matexpr/index.html \
		public_html/matexpr/matexpr.pdf

web-upload: doc/index.html 
	scp doc/index.html access:public_html/matexpr
	ssh access chmod a+r public_html/matexpr/index.html 

